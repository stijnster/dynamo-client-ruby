require 'dynamo/client/version'
require 'dynamo/error'
require 'digest'
require 'json'
require 'httparty'

module Dynamo

  class Client

    attr_reader :secret, :endpoint

    def initialize(secret:, endpoint: nil)
      @secret = secret
      @endpoint = endpoint || 'http://dynamo-server-api.eu-central-1.elasticbeanstalk.com'
    end

    def info
      result = ::HTTParty.get("#{@endpoint}/info")

      if result.code != 200
        raise ::Dynamo::Error, "remote server returned code #{result.code}"
      end

      return result.parsed_response
    end

    def pdf(url, options: nil, stub: false)
      request = { input: {} }
      request[:input][:url] = url
      bearer = ::Digest::SHA256.hexdigest("#{@secret}#{url}")

      if options.instance_of? Hash
        request[:input][:options] = {}
        options.each do |key, value|
          request[:input][:options][key.to_s.gsub(/_/, '-')] = value
        end
      end

      if stub
        return bearer, request
      else
        result = ::HTTParty.post("#{@endpoint}/pdf/from-url", body: request.to_json, headers:  { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{bearer}" })

        if result.code != 200
          raise ::Dynamo::Error, "remote server returned code #{result.code}"
        end

        return result.body
      end
    end

  end

end
