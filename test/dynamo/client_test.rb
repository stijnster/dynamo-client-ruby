require 'test_helper'

class Dynamo::ClientTest < Minitest::Test

  def test_that_it_has_a_version_number
    refute_nil ::Dynamo::Client::VERSION
  end

  def test_instance
    @dynamo = Dynamo::Client.new(secret: 'something')
    assert_instance_of Dynamo::Client, @dynamo
    assert_equal 'something', @dynamo.secret
    assert_equal 'http://dynamo-server-api.eu-central-1.elasticbeanstalk.com', @dynamo.endpoint
  end

  def test_info
    @dynamo = Dynamo::Client.new(secret: '26e32ff74f679398a334c2e3070a4b3605ad80ba2a1e722dc843ece4b82ee256300ec15c099c2a89b95cdfb5563dae2a25e1a2593a86202179019c889c65a3ec', endpoint: 'http://localhost:8080')

    info = @dynamo.info
    assert_equal 'Dynamo Server', info['description']
    assert_match(/\A2\.\d+\.\d+\.\d+\Z/, info['version'])
  end

  def test_pdf_request
    @dynamo = Dynamo::Client.new(secret: 'something')
    bearer, request = @dynamo.pdf('https://www.skylight.be/', options: { orientation: 'landscape', margin_left: '10mm' }, stub: true)

    assert_equal 'b1cbee9f63825c862ecc3c5d8c6008d7435649e3d5c81203bcdec67f4893cf94', bearer
    assert_equal 'https://www.skylight.be/', request[:input][:url]
    assert_equal({ "orientation" => "landscape", "margin-left" => "10mm" }, request[:input][:options])
  end

  def test_pdf
    @dynamo = Dynamo::Client.new(secret: '26e32ff74f679398a334c2e3070a4b3605ad80ba2a1e722dc843ece4b82ee256300ec15c099c2a89b95cdfb5563dae2a25e1a2593a86202179019c889c65a3ec', endpoint: 'http://localhost:8080')
    pdf = @dynamo.pdf('https://www.google.be/', options: { orientation: 'portrait', margin_left: '0mm' })
    assert_match(/\A%PDF-/, pdf)

    filename = File.join(File.realpath(File.join(__dir__, '..', 'tmp')), 'output.pdf')
    File.unlink(filename) if File.exist?(filename)
    File.open(filename, 'wb+') do |f|
      f.write pdf
    end

    assert File.exist?(filename)
  end

  def test_failing_pdf
    @dynamo = Dynamo::Client.new(secret: 'somefailingkey', endpoint: 'http://localhost:8080')
    assert_raises Dynamo::Error do
      @dynamo.pdf('https://www.skylight.be/', options: { orientation: 'landscape' })
    end
  end

end
